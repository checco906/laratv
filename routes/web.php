<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


//HomeController@index nome del controller @function nome della funzione

//->name('home'); nome dela route


//Route::middlware('auth')->group(function){}

Route::group(['middleware'=>'auth'],function(){


    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/search', 'SearchController@user')->name('search.user');

    Route::get('/follow/user{id}','UserController@follow');
    
    Route::get('/follow/serie{id}','UserController@followSerie');
    
    Route::get('/unfollow/serie{id}','UserController@unfollowSerie');

    Route::get('/unfollow/user{id}','UserController@unfollow');
    
    Route::get('/profile','UserController@profile');
    
    Route::put('/profile','UserController@profileUpdate');
    
    Route::get('/search-serie', 'SearchController@serie')->name('search.serie');
    
    //in tutte le root dove trovi serie e fau il model sulla classe che ti dico, se si prosegui senno 404
    Route::model('serie_int', App\Serie::class);
    
    Route::get('/series/{serie_int}', 'SerieController@show');
    
    



});
//put è simile al post 
