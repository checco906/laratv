<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( ['namespace' => 'Api'], function() {

    Route::post('authenticate', 'AuthenticateController@login');

    Route::post('registration', 'AuthenticateController@registration');

    Route::group( ['middleware' => 'auth:api'], function() {

        Route::post('refresh', 'AuthenticateController@refresh');

        Route::get( 'me', function() {

            // Recupero l'istanza dell'utente
            $user = auth('api')->user();

            // Creo l'istanza $userResource a cui passo l'istanza dell'utente autenticato
            $userResource = new App\Http\Resources\UserResource($user);

            return response()->json( $userResource );
            //            return response()->json( auth()->user() );
        });

    });
    
    Route::bind('series',function($id){
       
        $serie = App\Serie::where('thetvdb_id', $id)->first();
        
        if(empty($serie)){
            $serie = new App\Serie([
                'thetvdb_id' => $thetvdb_id,
            ]);
            $serie->fetchData();
            $serie->save();
        }
        
        return $serie;
        
    });
    
    Route::resource('series', 'SerieController', ['only'=>['index','show']]);
    Route::post('series/{series}/follow', 'SerieController@follow');

});


