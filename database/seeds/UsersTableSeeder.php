<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        App/User::create([
//            'name'=> 'eeee',
//            'email' => 'yyyy',
//        ]);
        factory(App\User::class, 5)->create();
    }
}
//le factories sono delle industrie che tirano fuori degli oggetti e vengono definite nella cartella database/factory