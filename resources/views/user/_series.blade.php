<div class="nav">

    @foreach($user->series as $serie)
    
   <div class="col-md-3">
     <a href="{{action('SerieController@show',[$serie->id])}}"
      <h5 class="text-center">{{$serie->title}}</h5>
       <img src="{{ $serie->poster_url ? : 'http://via.placeholder.com/340x500' }}" class="img-fluid">
   </div>
   
  @endforeach
  
</div>