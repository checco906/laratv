@extends('layouts/app')


@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">Profile</div>
        <div class="card-body">
            <div class="container">
                <div class="row my-2">
                    <div class="col-lg-8 order-lg-2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">{{ucwords(trans('custom.profile'))}}</a>
                            </li>
                            <li class="nav-item">
                                <a href="" data-target="#edit" data-toggle="tab" class="nav-link">{{ucwords(trans('custom.edit'))}}</a>
                            </li>
                            <li class="nav-item">
                                <a href="" data-target="#messages" data-toggle="tab" class="nav-link">{{ucwords(trans('custom.serie_tv'))}}</a>
                            </li>
                        </ul>
                        <div class="tab-content py-4">
                            <div class="tab-pane active" id="profile">
                                <div style="width:200px;margin:0;padding:0">
                                    <h5 class="mb-3 font-weight-bold">{{$user->name}} {{$user->surname}}</h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 class="font-weight-bold">NICKNAME</h6>
                                        <p>
                                            {{$user->nickname}}
                                        </p>
                                        <h6 class="font-weight-bold">EMAIL</h6>
                                        <p>
                                            {{$user->email}}
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary">
                                            Followers <span class="badge badge-light">{{ $user->followers()->count()}}</span>
                                        </button>
                                        <button type="button" class="btn btn-danger">
                                            Followed <span class="badge badge-light">{{ $user->followed()->count()}}</span>
                                        </button>
                                    </div>

                                </div>
                                <!--/row-->
                            </div>
                            <div class="tab-pane" id="messages">
                            @include('user/_series')
                                <div class="alert alert-info alert-dismissable">
                                    <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use this to show important messages to the user.
                                </div>
                                <table class="table table-hover table-striped">
                                    <tbody>                                    
                                        <tr>
                                            <td>
                                                <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
                                            </td>
                                        </tr>
                                    </tbody> 
                                </table>
                            </div>
                            <div class="tab-pane" id="edit">
                                <form method="post" action="{{ action('UserController@profileUpdate')}}">
                                   @method('put')
                                   
                                   @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ucfirst(trans('custom.name'))}}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $user->name) }}" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="surname" class="col-md-4 col-form-label text-md-right">Surname</label>

                                        <div class="col-md-6">
                                            <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname', $user->surname) }}" required autofocus>

                                            @if ($errors->has('surname'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('surname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="nickname" class="col-md-4 col-form-label text-md-right">Nickname</label>

                                        <div class="col-md-6">
                                            <input id="nickname" type="text" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" name="nickname" value="{{ old('nickname', $user->nickname) }}" required autofocus>

                                            @if ($errors->has('nickname'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('nickname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Salva
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 order-lg-1 text-center">
                        <img src="//placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="avatar">
                        <h6 class="mt-2">Upload a different photo</h6>
                        <label class="custom-file">
                            <input type="file" id="file" class="custom-file-input">
                            <span class="custom-file-control">Choose file</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
