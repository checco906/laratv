@extends('layouts/app')


@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">Search</div>

        <div class="card-body">

            <form method="get" action="{{ action('SearchController@user') }}">

                {{ csrf_field() }}

                <div class="form-group">
                    <div class="input-group">
                        <input id="q" type="text" class="form-control{{ $errors->has('q') ? 'is-invalid' : ''}}" name="q" value="{{request('q')}}" placeholder="Search">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                    </div>
                    @if ($errors->has('q'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('q')}}</strong>
                    </span>
                    @endif
                </div>
            </form>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name}}</td>
                        <td>{{ $user->surname }}}</td>
                        <td class="col-action">
                        @if(!auth()->user()->followed->contains('id',$user->id))
                        <!--utente non seguito-->
                            <a class="btn btn-success btn-sm" href="{{ action('UserController@follow',[$user->id])}}"><i class="fa fa-user-plus"></i></a>
                            @else
                            <!--                            utente seguito-->
                            <a class="btn btn-danger btn-sm" href="{{ action('UserController@unfollow',[$user->id])}}">
                                <i class="fa fa-user-times"></i>
                            </a>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!--            per non perdere il valore all'interno della search-->
            {{ $users->appends( request()->all())->links() }}

        </div>
    </div>
</div>
@endsection

<!--// {{ $users->links()}} crea la paginazione di bootstrap-->
