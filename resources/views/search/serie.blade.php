@extends('layouts/app')


@section('content')

<div class="container">
    <div class="card">
        <div class="card-header">Search</div>

        <div class="card-body">

            <form method="get" action="{{ action('SearchController@serie') }}">

                {{--                {{ csrf_field() }}--}}

                <div class="form-group">
                    <div class="input-group">
                        <input id="q" type="text" class="form-control{{ $errors->has('q') ? 'is-invalid' : ''}}" name="q" value="{{request('q')}}" placeholder="Search">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit">Search</button>
                        </div>
                    </div>
                    @if ($errors->has('q'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('q')}}</strong>
                    </span>
                    @endif
                </div>
            </form>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>First Aired</th>
                        <th>Banner</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($series as $serie)
                    <tr>
                        <td>{{ $serie->seriesName}}</td>
                        {{--                        <td>{{date('d/m/Y',strtotime( $serie->firstAired)) }}</td>--}}
                        <td>{{Carbon\Carbon::parse($serie->firstAired)->format('d/m/Y')}}</td>
                        <td><img src="http://www.thetvdb.com/banners/{{ $serie->banner }}" class="img-fluid"</td>
                        <td class="col-action">
                            @if(auth()->user()->series->contains('thetvdb_id',$serie->id))
                            <a class="btn btn-danger btn-sm" href="{{action('UserController@unfollowSerie',[$serie->id])}}">
                                <i class="fa fa-minus"></i>
                            </a> 
                            @else
                            <a class="btn btn-info btn-sm" href="{{action('UserController@followSerie',[$serie->id])}}">
                                <i class="fa fa-plus"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!--            per non perdere il valore all'interno della search-->


        </div>
    </div>
</div>
@endsection


