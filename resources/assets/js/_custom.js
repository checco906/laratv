$(function(){
    'use strict';
    
    
    var $messageSuccess = $('meta[name="message-success"]');
    
    var $messageError = $('meta[name="message-error"]');
    
    if($messageSuccess.length){
        toastr.success($messageSuccess.attr('content'));
    }
    if($messageError.length){
        toastr.error($messageError.attr('content'));
    }
//    toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!');
    
})