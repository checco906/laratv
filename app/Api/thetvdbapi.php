<?php

namespace App\Api;

use Ixudra\Curl\Facades\Curl;
use Cache;


class TheTvDbApi {

    public function login() {


        $response = Curl::to('https://api.thetvdb.com/login')
            ->withData(config('thetvdb.login'))
            ->asJson()
            ->post();
        return $response;
    }

    public function getToken() {

        return Cache::remember('the_tv_db_token', 1410, function () {

            return $this->login()->token;

        });
    }

    protected function getCurl($uri, $lang = 'it') {

        return Curl::to('https://api.thetvdb.com/'.ltrim($uri,'/'))

            ->asJson()
            ->withHeader('Accept-language: '.$lang)
            ->withHeader('Authorization: Bearer ' . $this->getToken());
    }

    public function search($value) {

        $response = $this->getCurl('/search/series')

            ->withData([

                'name' => $value
            ])
            ->get();

        return $response;
    }

    public function getSerie($id) {

        $response = $this->getCurl('/series/'.$id)
            ->get();

        return $response->data;
    }

    public function getPosters($id){

        $response = $this->getCurl('/series/'. $id . '/images/query','en')
            ->withData([

                'keyType' => 'poster'
            ])
            ->get();

        return collect(!empty($response->data) ? $response->data : [])->map(function($item){
            
            $item->fileName = 'http://www.thetvdb.com/banners/' .$item->fileName;
            $item->thumbnail = 'http://www.thetvdb.com/banners/' .$item->thumbnail;

            return $item;
        });
    }
    public function getPoster($id){

        return $this->getPosters($id)->sortByDesc('ratingsInfo.average')->first();
    }

}

