<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Api\TheTvDbApi;

class Serie extends Model
{
    protected $fillable = [
        'thetvdb_id',
        'title',
        'poster_url'
    ];

    public function fetchData(){
        if(!$this->thetvdb_id){
            throw new \Exception("Attributo 'thetvdb_id' non valorizzato",1);
        }

            $api = new TheTvDbApi;

        //2.a recupero i dati della serie dalle APi
        $info_serie = $api->getSerie($this->thetvdb_id);


        //2.b recupero i poster della serie dalle Api
        $poster = $api->getPoster($this->thetvdb_id);

        $this->fill([
            'title' => $info_serie->seriesName,
            'poster_url' => !empty($poster->fileName) ? $poster->fileName : '',
        ]);
    }
    
    public function getData(){
        
        $api = new TheTvDbApi;
        return $api->getSerie($this->thetvdb_id);
    }
}
