<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Api\TheTvDbAPi;

class SearchController extends Controller
{
    public function user(Request $request) {

        $query = User::query();
        
        $query->where('id', '!=', auth()->id());

        if($request->has('q')){

            //stringa% = inizia con asterisco
            //%stringa% = contiene stringa
            //%stringa = finisce con stringa
            $query->where(function($query) use ($request){
                
            $query
                ->where('name','like', '%' .$request->input('q'). '%')
                ->orwhere('surname','like', '%' .$request->input('q'). '%');
            });
        }

        //metodo paginazione,

        $users = $query->paginate();


        return view('search/user', compact('users'));
    }
    public function serie(Request $request) {
        
        $series = [];
        
        if($request->filled('q')){
        $api = new TheTvDbApi;
        $response = $api->search($request->input('q'));
        $series = empty($response->data) ? [] : $response->data;
        }
        return view('search/serie',compact('series'));
    }
}

//compact prende delle stringhe,  prende le variabili che hanno quel nome e crea una array associativo con la chiave e il valore dentro la variabile