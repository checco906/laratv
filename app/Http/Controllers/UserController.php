<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api\TheTvDbAPi;
use App\Serie;

class UserController extends Controller
{
    public function follow($id){

        //istanza dell'utente loggato
        $user = auth()->user();

        //
        if($id != $user->id && $user->followed()->where('user_id_2',$id)->count() == 0 ){


            //aggiungo la relazione
            $user -> followed()->attach($id);

        }
        //mando la relazione e torno alla pagina precedente

        return redirect()->back();
    }

    public function unfollow($id) {
        //istanza dell'utente loggato
        $user = auth()->user();


        //rimuovo la relazione
        $user -> followed()->detach($id);


        //rimuovo la relazione e torno alla pagina precedente

        return redirect()->back();
    }

    public function profile() {
        $user = auth()->user();
        return view('user/profile', compact('user'));
    }

    public function profileUpdate(Request $request) {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'nickname' => 'required|string|max:255|unique:users,nickname,'.auth()->id(), //unico senza contare l id dell'utente loggato
            'email' => 'required|string|email|max:255|unique:users,email,'.auth()->id(),
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        $user = auth()->user();

        $user->fill($request->except('password'));
        // Se l'utente modifica la password la aggiorno
        if($request->filled('password')){
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();


        return redirect()
            ->back()
            ->with('message-success','Profilo aggiornato');
    }

    //l'utente loggato segue una serie tv

    public function followSerie($thetvdb_id){
        $user = auth()->user();

        //1)Cercare serie con id esterno thetvdb_id
        $serie = Serie::where('thetvdb_id', $thetvdb_id)->first();
        //2)Se non esiste salvare i dati sul db
        if(empty($serie)){
           
            $serie = new Serie([
                'thetvdb_id' => $thetvdb_id
            ]);
            $serie->fetchData();
            
            $serie->save();

//            $api = new TheTvDbApi;
//
//            //2.a recupero i dati della serie dalle APi
//            $info_serie = $api->getSerie($thetvdb_id);
//
//
//            //2.b recupero i poster della serie dalle Api
//            $poster = $api->getPoster($thetvdb_id);
//
//
//            //2.c salvo sul database
//            $serie = new Serie;
//
//            //Crea istanzia filla e  salva
//            $serie = Serie::create([
//                'thetvdb_id' => $thetvdb_id,
//                'title' => $info_serie->seriesName,
//                'poster_url' => !empty($poster->fileName) ? $poster->fileName : '',
//            ]);

        }

        //3) Associazione utenete serie
        $user->series()->attach($serie->id);

        return redirect()->back();
    }
    public function unfollowSerie($thetvdb_id){
        //1)Recuperare l'utente loggato
        $user = auth()->user();
        
        //2) Recupero la serie dal database
        $serie = Serie::where('thetvdb_id',$thetvdb_id)->first();
        
        //3) Se trovo la serie tolgo l'associazione con l'utente
        if($serie){
            $user->series()->detach($serie->id);
            
        }
        
        //4)torno indietro
        return redirect()->back();
    }
}
