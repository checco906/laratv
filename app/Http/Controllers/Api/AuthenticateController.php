<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationRequest;
use App\User;

class AuthenticateController extends Controller
{
    //    public function authenticate(Request $request)
    //    {
    //        // grab credentials from the request
    //        $credentials = $request->only('email', 'password');
    //
    //        try {
    //            // attempt to verify the credentials and create a token for the user
    //            if (! $token = JWTAuth::attempt($credentials)) {
    //                return response()->json(['error' => 'invalid_credentials'], 401);
    //            }
    //        } catch (JWTException $e) {
    //            // something went wrong whilst attempting to encode the token
    //            return response()->json(['error' => 'could_not_create_token'], 500);
    //        }
    //
    //        // all good so return the token
    //        return response()->json(compact('token'));
    //    }
    public function login() {
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    protected function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    public function refresh() {
        return $this->respondWithToken(auth('api')->refresh());
    }

    public function registration(RegistrationRequest $request) {

        $user =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'surname' =>$request->surname,
            'nickname' => $request->nickname,
            'password' => bcrypt($request->password),
        ]);

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }
}
