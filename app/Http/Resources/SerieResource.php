<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SerieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
   
    public function toArray($request){
        return [

        'id' =>  $this->id,
        'titolo' =>  $this->titolo,
        'poster_url' =>  $this->poster_url
        
    ];
}
}